package controllers

import (
	"log"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm"

	"go-fiber-test/database"
	m "go-fiber-test/models"
	"regexp"
	"strconv"
)

func HelloTest(c *fiber.Ctx) error {
	return c.SendString("Hello, World! XD")

}

func HelloTestV2(c *fiber.Ctx) error {
	return c.SendString("Hello, World! XD V2")

}

func BodyParserTest(c *fiber.Ctx) error {
	p := new(m.Person)
	if err := c.BodyParser(&p); err != nil {
		return c.Status(fiber.StatusInternalServerError).SendString(("error"))
	}
	log.Println(p.Name)
	log.Println(p.Pass)
	str := p.Name + p.Pass
	log.Println(str)
	return c.JSON(p)

}

func ParamsTest(c *fiber.Ctx) error {

	x := c.Params("name")

	return c.SendString(x)

}

func QueryTest(c *fiber.Ctx) error {
	c.Query("search")

	a := c.Query("search")
	str := "my search is " + a
	return c.JSON(str)
}

func ValidTest(c *fiber.Ctx) error {
	//Connect to database

	user := new(m.User)
	log.Print(*user)
	log.Print(user)
	log.Print(&user)
	// user or &user
	if err := c.BodyParser(&user); err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"message": err.Error(),
		})
	}
	validate := validator.New()
	errors := validate.Struct(user)
	if errors != nil {
		return c.Status(fiber.StatusBadRequest).JSON(errors.Error())
	}
	return c.JSON(user)
}

// ============================================================================

func Factorial(c *fiber.Ctx) error {
	number := c.Params("number")
	s, err := strconv.Atoi(number)

	if err != nil {
		return c.SendStatus(fiber.StatusInternalServerError)
	}
	result := 1
	for i := 1; i <= s; i++ {
		result = result * i
	}
	fact := strconv.Itoa(result)
	return c.SendString(fact)

}

func Register(c *fiber.Ctx) error {

	user := new(m.RegisterDto)

	println("====")

	if err := c.BodyParser((user)); err != nil {
		print(err)
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"message": err.Error(),
		})

	}

	validate := validator.New()
	//add custom validator :)
	validate.RegisterValidation("mycustom", func(fl validator.FieldLevel) bool {
		return regexp.MustCompile(`^[a-zA-Z0-9_-]+$`).MatchString(fl.Field().String())
	})
	validate.RegisterValidation("web", func(fl validator.FieldLevel) bool {
		return regexp.MustCompile(`^[a-z0-9\.]+$`).MatchString(fl.Field().String())
	})
	errors := validate.Struct(user)
	if errors != nil {
		return c.Status(fiber.StatusBadRequest).JSON(errors.Error())
	}

	return c.JSON(user)
}

// for api v3
func TxAscii(c *fiber.Ctx) error {

	text := c.Query("tax_id")
	_ = text

	store := []rune{}
	storex := ""
	_ = store
	for _, v := range text {

		// storex = strconv.Itoa(int(v))
		store = append(store, v)
	}

	for i, v := range store {

		// storex += strconv.Itoa(int(v)) + " "

		storex += strconv.Itoa(int(v))

		if i < len(store)-1 {
			storex += " "
		}
	}

	return c.JSON(storex)

}

// ======= dog controller ======

// use scope
func DogIDGreaterThan100(db *gorm.DB) *gorm.DB {
	return db.Where("dog_id > ?", 100)
}

// ============= 7.1 Scope ====================
func GetDogsIdGt50Lt100(db *gorm.DB) *gorm.DB {
	return db.Where("dog_id > ? && dog_id < ?", 50, 100)

}

// =======================================

func GetDogs(c *fiber.Ctx) error {
	db := database.DBConn
	var dogs []m.Dogs

	// db.Find(&dogs) //delelete = null

	db.Scopes(DogIDGreaterThan100).Find(&dogs)
	return c.Status(200).JSON(dogs)
}

func GetDog(c *fiber.Ctx) error {
	db := database.DBConn
	search := strings.TrimSpace(c.Query("search"))
	var dog []m.Dogs

	result := db.Find(&dog, "dog_id = ?", search)

	// returns found records count, equals `len(users)
	if result.RowsAffected == 0 {
		return c.SendStatus(404)
	}
	return c.Status(200).JSON(&dog)
}

func AddDog(c *fiber.Ctx) error {
	//twst3
	db := database.DBConn
	var dog m.Dogs

	if err := c.BodyParser(&dog); err != nil {
		return c.Status(503).SendString(err.Error())
	}

	db.Create(&dog)
	return c.Status(201).JSON(dog)
}

func UpdateDog(c *fiber.Ctx) error {
	db := database.DBConn
	var dog m.Dogs
	id := c.Params("id")

	if err := c.BodyParser(&dog); err != nil {
		return c.Status(503).SendString(err.Error())
	}

	db.Where("id = ?", id).Updates(&dog)
	return c.Status(200).JSON(dog)
}

func RemoveDog(c *fiber.Ctx) error {
	db := database.DBConn
	id := c.Params("id")
	var dog m.Dogs

	result := db.Delete(&dog, id)

	// เช็คข้อมูลนั้นว่ามีหรือเปล่าว
	if result.RowsAffected == 0 {
		// ถ้าไม่มีให้ return 404 ออกไปว่าไม่มีข้อมูลนั้น
		return c.SendStatus(404)
	}

	return c.SendStatus(200)
}

func GetDogsJson(c *fiber.Ctx) error {
	db := database.DBConn
	var dogs []m.Dogs

	db.Find(&dogs) //10ตัว

	sumRed := 0
	sumGreen := 0
	sumPink := 0
	sumNone := 0
	_ = sumRed
	_ = sumGreen
	_ = sumPink
	_ = sumNone

	var dataResults []m.DogsRes
	for _, v := range dogs { //1 inet 112 //2 inet1 113
		typeStr := ""
		if v.DogID >= 10 && v.DogID <= 50 {
			typeStr = "red"
			sumRed++
		} else if v.DogID >= 100 && v.DogID <= 150 {
			typeStr = "green"
			sumGreen++
		} else if v.DogID >= 200 && v.DogID <= 250 {
			typeStr = "pink"
			sumPink++
		} else {
			typeStr = "no color"
			sumNone++
		}

		d := m.DogsRes{
			Name:  v.Name,  //inet1
			DogID: v.DogID, //113
			Type:  typeStr, //green
		}
		dataResults = append(dataResults, d)
		// sumAmount += v.Amount
	}

	r := m.ResultData{
		Data:        dataResults,
		Name:        "golang-test",
		Count:       len(dogs), //หาผลรวม,
		Sum_red:     sumRed,
		Sum_green:   sumGreen,
		Sum_pink:    sumPink,
		Sum_nocolor: sumNone,
	}
	return c.Status(200).JSON(r)
}

// 7.0.2
func GetDogsDelete(c *fiber.Ctx) error {
	db := database.DBConn
	var dogs []m.Dogs

	// db.Unscoped().Where("deleted_at is NOT NULL").Find(&dogs)
	db.Unscoped().Where("deleted_at").Find(&dogs)

	return c.Status(200).JSON(dogs)

}

// === 7.0.1 Company CRUD =====
// get all Companys
func GetCompanys(c *fiber.Ctx) error {
	db := database.DBConn
	var companys []m.Company

	db.Find(&companys)
	return c.Status(200).JSON(companys)

}

// get Company by id
func GetCompany(c *fiber.Ctx) error {
	db := database.DBConn
	id, err := strconv.Atoi(c.Params("id"))

	if err != nil {
		return c.SendStatus(404)
	}
	var company m.Company
	result := db.Find(&company, id)

	if result.Error != nil {
		return result.Error

	}

	return c.Status(200).JSON(company)

}

// add Company
func AddCompany(c *fiber.Ctx) error {
	db := database.DBConn

	var comapny m.Company

	if err := c.BodyParser(&comapny); err != nil {
		return c.Status(503).SendString(err.Error())
	}

	db.Create(&comapny)
	return c.Status(201).JSON(comapny)

}

// delete Company
func DeleteCompany(c *fiber.Ctx) error {
	db := database.DBConn

	id := c.Params("id")

	var company m.Company

	result := db.Delete(&company, id)

	if result.RowsAffected == 0 {
		return c.SendStatus(404)
	}

	return c.JSON(fiber.Map{"message": "delete successful"})

}

// update Company
func UpdateCompany(c *fiber.Ctx) error {
	db := database.DBConn

	id := c.Params("id")

	var company m.Company

	if err := c.BodyParser(&company); err != nil {
		return c.Status(503).SendString(err.Error())
	}

	// db.Where("id = ?", id).Update(&company)
	db.Where("id = ?", id).Updates(&company)

	return c.Status(200).JSON(company)
}

// ===== 7.1 :)
func GetDog50_100(c *fiber.Ctx) error {
	db := database.DBConn
	var dogs []m.Dogs

	db.Scopes(GetDogsIdGt50Lt100).Find(&dogs)

	return c.Status(200).JSON(dogs)

}

// ================ Profiles ===========================

// Get  User Profiles

func GetProfiles(c *fiber.Ctx) error {
	var profiles []m.Profile
	db := database.DBConn

	db.Find(&profiles)

	return c.Status(200).JSON(profiles)

}

// Get  User Profiles By Id
func GetProfile(c *fiber.Ctx) error {
	var profile m.Profile
	id, err := strconv.Atoi(c.Params("id"))
	db := database.DBConn
	if err != nil {
		return c.SendStatus(404)
	}

	result := db.Find(&profile, id)

	if result.Error != nil {
		return result.Error

	}

	return c.Status(200).JSON(profile)

}

// Add  User Profiles
func AddProfile(c *fiber.Ctx) error {
	var profile m.Profile
	db := database.DBConn

	if err := c.BodyParser(&profile); err != nil {
		return c.Status(503).SendString(err.Error())

	}

	db.Create(&profile)

	return c.Status(201).JSON(profile)

}

// Update  User Profiles
func UpdateProfile(c *fiber.Ctx) error {
	var profile m.Profile
	id, err := strconv.Atoi(c.Params("id"))

	if err != nil {
		return c.SendStatus(fiber.StatusInternalServerError)
	}
	db := database.DBConn

	if err := c.BodyParser(&profile); err != nil {
		return c.Status(503).SendString(err.Error())
	}

	db.Where("id = ?", id).Updates(&profile)

	return c.Status(200).JSON(profile)

}

// Delete  User Profiles

func DeleteProfile(c *fiber.Ctx) error {

	db := database.DBConn

	id, err := strconv.Atoi(c.Params("id"))
	if err != nil {
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	var profile m.Profile
	result := db.Delete(&profile, id)

	// เช็คข้อมูลนั้นว่ามีหรือเปล่าว
	if result.RowsAffected == 0 {
		return c.SendStatus(404)
	}

	return c.JSON(fiber.Map{"message": "delete successful"})

}

// get Profile from any ages
func GetProfileAnyAges(c *fiber.Ctx) error {
	var profiles []m.Profile
	db := database.DBConn

	_ = profiles
	_ = db

	sumGenZ := 0
	sumGenY := 0
	sumGenX := 0
	sumBabyBoomer := 0
	sumGI := 0

	db.Find(&profiles)
	var dataResult []m.ProfileRes
	_ = dataResult
	for _, value := range profiles {

		genStr := ""

		if value.Age > 75 {
			genStr = "G.I Generation"
			sumGI++
		} else if value.Age >= 24 && value.Age <= 41 {
			genStr = "Gen Y"
			sumGenY++

		} else if value.Age >= 42 && value.Age <= 56 {
			genStr = "Gen X"
			sumGenX++
		} else if value.Age >= 57 && value.Age <= 75 {
			genStr = "Baby Boomer"
			sumBabyBoomer++
		} else {
			genStr = "Gen Z"
			sumGenZ++
		}

		p := m.ProfileRes{
			EmployeeID: value.EmployeeID,
			Name:       value.Name,
			Lastname:   value.Lastname,
			Birthday:   value.Birthday,
			Age:        value.Age,
			Email:      value.Email,
			Tel:        value.Tel,
			Gen:        genStr,
		}

		dataResult = append(dataResult, p)

	}

	r := m.ProfileAgesResult{
		Data:           dataResult,
		Name:           "profile-ages",
		Count:          len(profiles),
		Sum_genz:       sumGenZ,
		Sum_geny:       sumGenY,
		Sum_genx:       sumGenX,
		Sum_babyboomer: sumBabyBoomer,
		Sum_gi:         sumGI,
	}

	return c.Status(200).JSON(r)

}

// search master
func SearchProfile(c *fiber.Ctx) error {

	db := database.DBConn
	_ = db
	var profiles []m.Profile

	empId := c.Query("employee_id")
	name := c.Query("name")
	lastname := c.Query("lastname")

	println(empId)
	println(name)
	println(lastname)
	if empId != "" && name != "" && lastname != "" {
		print("===Here===")
		db.Where("employee_id = ? && name = ? && lastname = ?", empId, name, lastname).Find(&profiles)
	} else if empId != "" && name != "" {
		db.Where("employee_id = ? && name = ? ", empId, name).Find(&profiles)
	} else if empId != "" && lastname != "" {
		db.Where("employee_id = ? && lastname = ? ", empId, lastname).Find(&profiles)
	} else if name != "" && lastname != "" {
		db.Where("name = ? && lastname = ?", name, lastname).Find(&profiles)
	} else if empId != "" {
		db.Where("employee_id = ?", empId).Find(&profiles)
	} else if name != "" {
		db.Where("name = ?", name).Find(&profiles)
	} else if lastname != "" {
		db.Where("lastname = ?", lastname).Find(&profiles)
	}

	// log.Fatal(result)

	// m := c.Queries()

	// println(" =")
	// println(m["employee_id"])
	// println(" =")

	// if m["employee_id"] == "" {
	// 	print("ss")
	// }

	return c.JSON(profiles)

}

// normal search
func SearchNormal(c *fiber.Ctx) error {

	db := database.DBConn
	_ = db
	var profiles []m.Profile
	_ = profiles

	search := c.Query("search")

	println(search)

	// db.Where("?", search).Find(&profiles)
	db.Where("(employee_id = ? || name = ? || lastname = ? )&& deleted_at is NULL", search, search, search).Find(&profiles)

	return c.JSON(profiles)

}
