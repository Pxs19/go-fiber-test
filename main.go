package main

import (
	//"ชื่อ project(ใน gomod มี)/ชื่อ package"
	"fmt"
	"go-fiber-test/database"
	m "go-fiber-test/models"
	"go-fiber-test/routes"

	"github.com/gofiber/fiber/v2"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

type Person struct {
	Name string `json:"name"`
	Pass string `json:"pass"`
}

func initDatabase() {

	dsn := fmt.Sprintf(
		"%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=true&loc=Local",
		"root",
		"",
		"127.0.0.1",
		"3306",
		"golang_test",
	)

	var err error
	database.DBConn, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})

	if err != nil {
		print(err)
	}

	fmt.Println("Database has Connected !!")
	// migrate
	database.DBConn.AutoMigrate(&m.Dogs{}, &m.Company{}, &m.Profile{})

}

func main() {

	app := fiber.New()
	initDatabase()
	// log.Fatal(db)

	routes.InetRoutes(app)

	// error status: 400 ส่งค่ามาผิด, 401 Token ผิด(Unauthorized), 403 login ได้แต่สิทธิ์ไม่ถึง, 404 ไม่เจอข้อมูล (record not found)
	// error status: 200 success, 201 create success
	// error: 500 internal server error/ server error, 502 server ล่ม หรือไม่ได้รัน
	app.Listen(":8080")

	// test
}
