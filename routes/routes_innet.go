package routes

import (
	c "go-fiber-test/controllers"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/basicauth"
)

func InetRoutes(app *fiber.App) {

	api := app.Group("api")
	v1 := api.Group("/v1")

	// ===== Profile ==========
	profile := v1.Group("/profile")
	_ = profile
	profile.Get("/", c.GetProfiles)

	// ========== middleware =============
	app.Use(basicauth.New(basicauth.Config{
		Users: map[string]string{
			"testgo": "23012023",
		},
	}))

	// ===== Profile CRUD ==========
	profile.Get("/any/age", c.GetProfileAnyAges)
	profile.Get("resource/search", c.SearchProfile)
	profile.Get("/filter", c.SearchNormal)
	profile.Post("/", c.AddProfile)
	profile.Get("/:id", c.GetProfile)
	profile.Patch("/:id", c.UpdateProfile)
	profile.Delete("/:id", c.DeleteProfile)

	// Group routes -> /api/v1/.....
	v1.Get("/hello2", c.HelloTest)
	v1.Post("/", c.BodyParserTest)
	v1.Get("/user/:name", c.ParamsTest)
	v1.Post("/inet", c.QueryTest)
	v1.Post("/valid", c.ValidTest)
	// =========== hw ==============
	v1.Get("/fact/:number", c.Factorial)
	v1.Post("register", c.Register)
	// =========== hw ==============

	v2 := api.Group("/v2")
	// _ = v2
	v2.Get("/hello2", c.HelloTestV2)

	// =========== hw ================
	v3 := api.Group("v3")
	myname := v3.Group("james")
	myname.Get("/", c.TxAscii)
	// =========== hw ==============

	// ==== dog =====
	//CRUD dogs
	dog := v1.Group("/dog")
	dog.Get("/", c.GetDogs)         //a
	dog.Get("/filter", c.GetDog)    //a
	dog.Get("/json", c.GetDogsJson) //a
	dog.Post("/", c.AddDog)         //a
	dog.Put("/:id", c.UpdateDog)    //a
	dog.Delete("/:id", c.RemoveDog) //a
	// 7.0.2 getDogDelete
	dog.Get("/dogdel", c.GetDogsDelete)
	// 7.1
	dog.Get("/gt50lt100", c.GetDog50_100)

	// GetDogsJson -> เอาข้อมูลมาเปลี่ยนแปลงนิดหน่อย

	// 7.0.1 CRUD -> Company
	company := v1.Group("/company")
	_ = company
	company.Get("/", c.GetCompanys)
	company.Post("/", c.AddCompany)
	company.Get("/:id", c.GetCompany)
	company.Patch("/:id", c.UpdateCompany)
	company.Delete("/:id", c.DeleteCompany)

}
