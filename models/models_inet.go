package models

import "gorm.io/gorm"

type Person struct {
	Name string `json:"name"`
	Pass string `json:"pass"`
}

type User struct {
	Name     string `json:"name" validate:"required,min=3,max=32"`
	IsActive *bool  `json:"isactive" validate:"required"`
	Email    string `json:"email,omitempty" validate:"required,email,min=3,max=32"`
}

// alphanum -> A-Z, a-z, 0-9
// - -> -
type RegisterDto struct {
	Email    string `json:"email" validate:"required,email"`
	Username string `json:"username" validate:"required,mycustom"`
	Password string `json:"password" validate:"required,min=2,max=20"`
	LineId   string `json:"lineId" validate:"required"`
	Phone    string `json:"phone" validate:"required,e164"`
	Business string `json:"business" validate:"required"`
	Webname  string `json:"webname" validate:"required,web,gte=2,lte=30"`
}

type Dogs struct {
	gorm.Model
	Name  string `json:"name"`
	DogID int    `json:"dog_id"`
}

type DogsRes struct {
	Name  string `json:"name"`
	DogID int    `json:"dog_id"`
	Type  string `json:"type"`
}

type ResultData struct {
	Data        []DogsRes `json:"data"`
	Name        string    `json:"name"`
	Count       int       `json:"count"`
	Sum_red     int       `json:"sum_red"`
	Sum_green   int       `json:"sum_green"`
	Sum_pink    int       `json:"sum_pink"`
	Sum_nocolor int       `json:"sum_nocolor"`
}

type Company struct {
	gorm.Model
	Name    string `json:"name"`
	Tel     string `json:"tel"`
	Country string `json:"country"`
}

// === User Model ====
type Profile struct {
	gorm.Model
	EmployeeID string `json:"employee_id"`
	Name       string `json:"name"`
	Lastname   string `json:"lastname"`
	Birthday   string `json:"birthday"`
	Age        int    `json:"age"`
	Email      string `json:"email"`
	Tel        string `json:"tel"`
}

type ProfileRes struct {
	EmployeeID string `json:"employee_id"`
	Name       string `json:"name"`
	Lastname   string `json:"lastname"`
	Birthday   string `json:"birthday"`
	Age        int    `json:"age"`
	Email      string `json:"email"`
	Tel        string `json:"tel"`
	Gen        string `json:"gen"`
}

type ProfileAgesResult struct {
	Data           []ProfileRes `json:"data"`
	Name           string       `json:"name"`
	Count          int          `json:"count"`
	Sum_genz       int          `json:"sum_genz"`
	Sum_geny       int          `json:"sum_geny"`
	Sum_genx       int          `json:"sum_genx"`
	Sum_babyboomer int          `json:"sum_babyboomer"`
	Sum_gi         int          `json:"sum_gi"`
}
